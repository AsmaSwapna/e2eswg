import logging
import random
import uuid, json
from settings import *
from network_manager.ovs_handler import *
from slice_creator import logs

def create_resource(slice_part_id):
    # get data from file
    with open(datastore_path) as json_file: slice_mappings = json.load(json_file)
    # build array of used keys
    used_keys = []
    for slice in slice_mappings:
        used_keys.append(slice["gen_key"])
    # generate key and check if exists
    gen_key = random.randint(10, 5000)
    while str(gen_key) in used_keys:
        gen_key = random.randint(10, 5000)

    gen_key = str(gen_key)
    bridge_name = "bridge_" + gen_key

    used_keys.append(gen_key)
    slice_mappings[slice_part_id] = {"bridge_name" : bridge_name, "gen_key" : gen_key}

    # update datastore
    with open(datastore_path, 'w') as outfile:  json.dump(slice_mappings, outfile)

    logs.logger.info(create_bridge(bridge_name))
    logs.logger.info(set_vxlan(bridge_name, gen_key))

    return bridge_name

def remove_resource(slice_part_id):
    # get data from file
    with open(datastore_path) as json_file: slice_mappings = json.load(json_file)
    logs.logger.info(slice_mappings)

    bridge_name = slice_mappings[slice_part_id]["bridge_name"]
    gen_key = slice_mappings[slice_part_id]["gen_key"]
    # delete slice of datastore
    if(slice_mappings.get(slice_part_id)):
        del slice_mappings[slice_part_id]
    # delete bridge
    delete_bridge(bridge_name)
    # update datastore
    with open(datastore_path, 'w') as outfile:  json.dump(slice_mappings, outfile)
    return 1
